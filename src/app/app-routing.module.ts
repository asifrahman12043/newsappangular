import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ArtsnewsComponent } from './artsnews/artsnews.component';
import { UsComponent } from './us/us.component';
import { ScienceComponent } from './science/science.component';
import { WorldComponent } from './world/world.component';

const routes: Routes = [
  {path:'', component:HomeComponent},
  {path:'arts', component:ArtsnewsComponent},
  {path:'us', component:UsComponent},
  {path:'science', component:ScienceComponent},
  {path:'world', component:WorldComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
