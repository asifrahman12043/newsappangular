import { Component, OnInit } from '@angular/core';
import { NewsapiservicesService } from '../service/newsapiservices.service'
import Localbase from 'localbase'

@Component({
  selector: 'app-artsnews',
  templateUrl: './artsnews.component.html',
  styleUrls: ['./artsnews.component.css']
})
export class ArtsnewsComponent implements OnInit {

  constructor(private _services: NewsapiservicesService) { }

  artsNewsDisplay: any[]
  newsDetails: object = {};
  isShowDetailsView: boolean = false;
  ngOnInit(): void {


    this._services.callArtsPageNews()

    let db = new Localbase('db')
    db.collection('arts_page_news').doc('arts_page_news').get().then(document => {
      var arts_page_news = document.results.results
      this.artsNewsDisplay = arts_page_news
    })
  }


  onClick(details: any) {
    if (details) {
      console.log(details);
      this.newsDetails = details;

      this.isShowDetailsView = true;
    }

  }

}
