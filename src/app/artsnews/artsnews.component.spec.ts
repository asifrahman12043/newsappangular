import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtsnewsComponent } from './artsnews.component';

describe('ArtsnewsComponent', () => {
  let component: ArtsnewsComponent;
  let fixture: ComponentFixture<ArtsnewsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArtsnewsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtsnewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
