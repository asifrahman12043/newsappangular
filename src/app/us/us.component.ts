import { Component, OnInit } from '@angular/core';
import {NewsapiservicesService} from '../service/newsapiservices.service'
import Localbase from 'localbase'

@Component({
  selector: 'app-us',
  templateUrl: './us.component.html',
  styleUrls: ['./us.component.css']
})
export class UsComponent implements OnInit {

  
  constructor(private _services: NewsapiservicesService) { }


  usInfoDisplay: any[]
  newsDetails:object={};
  isShowDetailsView:boolean=false;

  ngOnInit(): void {
 
    

    this._services.callUspageNews()

    let db = new Localbase('db')
    db.collection('us_page_news').doc('us_page_news').get().then(document => {
      var us_page_news = document.results.results
      this.usInfoDisplay = us_page_news
    })
  }

  onClick(details:any){
    if(details){
      console.log(details);
      this.newsDetails=details;

      this.isShowDetailsView=true;
    }
    
  }


}
