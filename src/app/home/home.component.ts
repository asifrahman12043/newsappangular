import { Component, OnInit } from '@angular/core';
import { NewsapiservicesService } from '../service/newsapiservices.service';
import Localbase from 'localbase'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private _services: NewsapiservicesService) { }


  homepageDisplay: any[]
  newsDetails:object={};
  isShowDetailsView:boolean=false;

  ngOnInit(): void {

   
    this._services.callhomePage()

     let db = new Localbase('db')
    db.collection('home_page_news').doc('home_page_news').get().then(document => {
      var home_page_news = document.results.results
      this.homepageDisplay = home_page_news
    })
  }

  onClick(details:any){
    if(details){
      console.log(details);
      this.newsDetails=details;

      this.isShowDetailsView=true;
    }
    
  }

  
}
