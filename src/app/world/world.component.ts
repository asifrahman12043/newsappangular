import { Component, OnInit } from '@angular/core';
import { NewsapiservicesService } from '../service/newsapiservices.service'
import Localbase from 'localbase'

@Component({
  selector: 'app-world',
  templateUrl: './world.component.html',
  styleUrls: ['./world.component.css']
})
export class WorldComponent implements OnInit {

  constructor( private _services:NewsapiservicesService) { }

  worldinfoNewsDisplay:any[]
  newsDetails:object={};
  isShowDetailsView:boolean=false;
  ngOnInit(): void {

    

    this._services.callWorldPageNews()

    let db = new Localbase('db')
    db.collection('world_page_news').doc('world_page_news').get().then(document => {
      var world_page_news = document.results.results
      this.worldinfoNewsDisplay = world_page_news
    })

  }


  onClick(details:any){
    if(details){
      console.log(details);
      this.newsDetails=details;

      this.isShowDetailsView=true;
    }
    
  }
}
