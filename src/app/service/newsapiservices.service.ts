import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import Localbase from 'localbase'

@Injectable({
  providedIn: 'root'
})
export class NewsapiservicesService {
  private db ; 

  constructor(private _http: HttpClient ) {
    
     this.db = new Localbase('db')
 
  }


  callhomePage(){
    const home_page_news = this._http.get("https://api.nytimes.com/svc/topstories/v2/home.json?api-key=pEFWWtfkOemmsPGrBAelDuMvFbQO9RLO").toPromise();
    home_page_news.then((results) => {
  
     return this.db.collection('home_page_news')
        .set([{
          results,
          _key: 'home_page_news'
        }
        ], { keys: true })
  
    }).catch((error) => {
      console.log("Promise rejected with " + JSON.stringify(error));
    });
  }
 
  

  callUspageNews(){
    const us_page_news = this._http.get("https://api.nytimes.com/svc/topstories/v2/us.json?api-key=pEFWWtfkOemmsPGrBAelDuMvFbQO9RLO").toPromise();
    us_page_news.then((results) => {
  
      return  this.db.collection('us_page_news')
        .set([{
          results,
          _key: 'us_page_news'
        }
        ], { keys: true })
  
    }).catch((error) => {
      console.log("Promise rejected with " + JSON.stringify(error));
    });
  }


  callSciencePageNews(){
    const science_page_news = this._http.get("https://api.nytimes.com/svc/topstories/v2/science.json?api-key=pEFWWtfkOemmsPGrBAelDuMvFbQO9RLO").toPromise();
    science_page_news.then((results) => {
  
      return  this.db.collection('science_page_news')
        .set([{
          results,
          _key: 'science_page_news'
        }
        ], { keys: true })
  
    }).catch((error) => {
      console.log("Promise rejected with " + JSON.stringify(error));
    });
  }

  


  callWorldPageNews(){
    const world_page_news = this._http.get("https://api.nytimes.com/svc/topstories/v2/world.json?api-key=pEFWWtfkOemmsPGrBAelDuMvFbQO9RLO").toPromise();
    world_page_news.then((results) => {
  
      return  this.db.collection('world_page_news')
        .set([{
          results,
          _key: 'world_page_news'
        }
        ], { keys: true })
  
    }).catch((error) => {
      console.log("Promise rejected with " + JSON.stringify(error));
    });
  }


  callArtsPageNews(){
    const arts_page_news = this._http.get("https://api.nytimes.com/svc/topstories/v2/arts.json?api-key=pEFWWtfkOemmsPGrBAelDuMvFbQO9RLO").toPromise();
    arts_page_news.then((results) => {
  
      return  this.db.collection('arts_page_news')
        .set([{
          results,
          _key: 'arts_page_news'
        }
        ], { keys: true })
  
    }).catch((error) => {
      console.log("Promise rejected with " + JSON.stringify(error));
    });
  }

}
