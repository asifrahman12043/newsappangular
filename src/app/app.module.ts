import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from "@angular/common/http";
import { NewsapiservicesService } from './service/newsapiservices.service';
import { ArtsnewsComponent } from './artsnews/artsnews.component';
import { NewsDetailsComponent } from './news-details/news-details.component';
import { SliderModule } from 'angular-image-slider';
import { UsComponent } from './us/us.component';
import { WorldComponent } from './world/world.component';
import { ScienceComponent } from './science/science.component';
import { HomeComponent } from './home/home.component';

 
@NgModule({
  declarations: [
    AppComponent,
    ArtsnewsComponent,
    NewsDetailsComponent,
    UsComponent,
    WorldComponent,
    ScienceComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SliderModule
  ],
  providers: [NewsapiservicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
