import { Component, OnInit } from '@angular/core';
import { NewsapiservicesService } from '../service/newsapiservices.service'
import Localbase from 'localbase'

@Component({
  selector: 'app-science',
  templateUrl: './science.component.html',
  styleUrls: ['./science.component.css']
})
export class ScienceComponent implements OnInit {

  constructor( private _services:NewsapiservicesService) { }

  ScienceNewsDisplay:any[]
  newsDetails:object={};
  isShowDetailsView:boolean=false;
  ngOnInit(): void {

    

    this._services.callSciencePageNews()

    let db = new Localbase('db')
        db.collection('science_page_news').doc('science_page_news').get().then(document => {
          var science_page_news = document.results.results
          this.ScienceNewsDisplay = science_page_news
        })
  }


  onClick(details:any){
    if(details){
      console.log(details);
      this.newsDetails=details;

      this.isShowDetailsView=true;
    }
    
  }

}
